#[macro_use]
extern crate log;
mod agents;
mod algo;
mod ccs;
mod io;
mod nn_model;
use clap::{App, Arg, SubCommand};

mod training {
    use crate::agents::{Agent, MCTSAgent, NNMCTSAgent, RandAgent};
    use crate::algo::NNMCTS;
    use crate::ccs;
    use crate::io::AsImg;
    use itertools::izip;
    use rand::prelude::*;
    use rayon::prelude::*;
    use std::sync::{Arc, Mutex};
    use tensorflow as tf;

    const TRAINING_MODEL_PATH: &str = "./training_model/";
    const BASELINE_MODEL_PATH: &str = "./baseline_model/";
    const PLAYOUT_CAPS: [(usize, usize); 2] = [(150, 4), (500, 1)];
    const GENERATIONS_PATH: &str = "./generations/";
    const STOP_SELF_PLAY_PATH: &str = "./stop-selfplay";

    pub fn train_epoch(selfplay_times: usize, selfplay_threads: usize) {
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(selfplay_threads)
            .build()
            .unwrap();
        pool.install(|| _train_epoch(selfplay_times, selfplay_threads));
    }

    pub fn _train_epoch(selfplay_times: usize, selfplay_threads: usize) {
        create_testing_model_if_not_exists();

        let mut agent =
            NNMCTSAgent::new(TRAINING_MODEL_PATH).expect("Failed to create NNMCTS agent");

        // Create pool of agents
        let agent_pool = Arc::new(Mutex::new(vec![]));
        let ongoing = Arc::new(Mutex::new(std::collections::HashSet::new()));
        for _ in 0..selfplay_threads {
            agent_pool.lock().unwrap().push(
                NNMCTSAgent::new(TRAINING_MODEL_PATH).expect("Failed to create NNMCTS agent"),
            );
        }

        // (state, pi_dist, winner) tuples
        let next_selfplay_round = Arc::new(Mutex::new(0));
        let selfplay_record: Arc<
            Mutex<Vec<(ccs::State, Vec<f32>, Option<ccs::Player>, [u16; 2])>>,
        > = Arc::new(Mutex::new(vec![]));

        let thread_handles: Vec<_> = (0..selfplay_threads)
            .map(|tid| {
                let next_selfplay_round = next_selfplay_round.clone();
                let selfplay_record = selfplay_record.clone();
                let agent_pool = agent_pool.clone();
                let ongoing = ongoing.clone();
                std::thread::spawn(move || loop {
                    // Get round number
                    let self_play_round = {
                        let mut next_selfplay_round = next_selfplay_round.lock().unwrap();
                        let round = Some(*next_selfplay_round);
                        *next_selfplay_round += 1;

                        round
                    };

                    let self_play_round = match self_play_round {
                        Some(self_play_round) => self_play_round,
                        None => {
                            break;
                        }
                    };

                    if self_play_round >= selfplay_times {
                        break;
                    }

                    let mut states = vec![];
                    let mut pi_dists = vec![];
                    let mut winners = vec![];
                    let mut scores = vec![];

                    loop {
                        info!("Self-play round {} started", self_play_round);

                        let is_run_game_success = {
                            // Get one agent from the pool
                            let (agent, agent_pool_len) = {
                                let mut agent_pool = agent_pool.lock().unwrap();
                                (agent_pool.pop(), agent_pool.len())
                            };

                            if let Some(mut agent) = agent {
                                info!(
                                    "Self-play round {} got agent ({} remaining in the pool)",
                                    self_play_round, agent_pool_len
                                );
                                ongoing.lock().unwrap().insert(self_play_round);
                                info!("Ongoing: {:?}", ongoing.lock().unwrap());
                                let run_game_result = train_run_game(
                                    &mut states,
                                    &mut pi_dists,
                                    &mut winners,
                                    &mut scores,
                                    &mut agent,
                                );

                                // Push the agent back
                                {
                                    let mut agent_pool = agent_pool.lock().unwrap();
                                    agent_pool.push(agent);
                                    ongoing.lock().unwrap().remove(&self_play_round);
                                }

                                // Return the game result
                                run_game_result
                            } else {
                                error!(
                                    "Self-play round {} failed to get agent from the pool",
                                    self_play_round
                                );
                                std::thread::sleep(std::time::Duration::from_millis(1000));
                                false
                            }
                        };

                        if is_run_game_success {
                            info!("Self-play round {} ended", self_play_round);
                            break;
                        } else {
                            warn!("Restarting game {}, which was stuck", self_play_round);
                            states.clear();
                            pi_dists.clear();
                            winners.clear();
                            scores.clear();
                        }
                    }

                    assert_eq!(states.len(), pi_dists.len());
                    assert_eq!(states.len(), winners.len());
                    assert_eq!(states.len(), scores.len());

                    {
                        let mut selfplay_record = selfplay_record.lock().unwrap();
                        selfplay_record.extend(izip!(
                            states.into_iter(),
                            pi_dists.into_iter(),
                            winners.into_iter(),
                            scores.into_iter()
                        ))
                    }
                })
            })
            .collect();
        for handle in thread_handles.into_iter() {
            if let Err(e) = handle.join() {
                error!("Error joining thread handle: {:?}", e);
            }
        }

        let selfplay_record = selfplay_record.lock().unwrap();
        drop(agent_pool);

        info!("Training");

        let train_count = selfplay_record.len() * 5;
        let mut rng = thread_rng();
        let mut losses = vec![];
        losses.reserve(train_count);
        for train_case in 0..train_count {
            let should_print_this_train_case = train_case % 100 == 0;
            if should_print_this_train_case {
                info!("Training sample {}", train_case);
            }

            let idx = rng.gen_range(0..(selfplay_record.len()));

            let (state, pi_dist, winner, scores) = &selfplay_record[idx];
            let me = state.get_next_player();

            let (state_tensor, drawn_tensor, scores_tensor) = state.as_tensor();
            let pi_dist_tensor = tf::Tensor::new(&[3584])
                .with_values(pi_dist)
                .expect("Failed to construct pi dist tensor");
            let value = if let Some(winner) = winner {
                if *winner == me {
                    1f32
                } else {
                    0f32
                }
            } else {
                0.25f32
            };
            let value_tensor = tf::Tensor::new(&[])
                .with_values(&[value])
                .expect("Failed to construct value tensor");
            let scores_t_tensor = ccs::utils::scores_to_tensor(scores.clone(), me);

            {
                let mut op = agent.get_model().operate();
                op.add_feed("train", "state", &state_tensor);
                op.add_feed("train", "drawn", &drawn_tensor);
                op.add_feed("train", "cur_scores", &scores_tensor);
                op.add_feed("train", "policy_t", &pi_dist_tensor);
                op.add_feed("train", "value_t", &value_tensor);
                op.add_feed("train", "scores_t", &scores_t_tensor);

                let loss_token = op.request_fetch("train", "output_0");
                let value_token = op.request_fetch("train", "output_1");
                let policy_token = op.request_fetch("train", "output_2");
                let scores_token = op.request_fetch("train", "output_3");
                op.run().expect("Failed to run train function");
                if let Ok(loss) = op.fetch(loss_token) {
                    let loss: f32 = loss[0];
                    losses.push(loss);
                    if should_print_this_train_case {
                        info!("Loss: {}", loss);
                    }
                }
                if let Ok(value) = op.fetch(value_token) {
                    let value: f32 = value[0];
                    if should_print_this_train_case {
                        info!("value: {}", value);
                    }
                }
                if let Ok(policy) = op.fetch(policy_token) {
                    let policy: f32 = policy[0];
                    if should_print_this_train_case {
                        info!("policy: {}", policy);
                    }
                }
                if let Ok(scores) = op.fetch(scores_token) {
                    let scores: f32 = scores[0];
                    if should_print_this_train_case {
                        info!("scores: {}", scores);
                    }
                }
            }
        }

        let avg_loss: f32 = losses
            .iter()
            .cloned()
            .map(|loss| loss / (train_count as f32))
            .sum();
        info!("Avg loss: {}", avg_loss);

        agent.save_model(TRAINING_MODEL_PATH);
    }

    fn train_run_game(
        states: &mut Vec<ccs::State>,
        pi_dists: &mut Vec<Vec<f32>>,
        winners: &mut Vec<Option<ccs::Player>>,
        scores: &mut Vec<[u16; 2]>,
        agent: &mut NNMCTSAgent,
    ) -> bool {
        let mut state = ccs::State::new();

        // Number of elements pushed into `states` and `pi_dists` so far
        let mut round_count = 0;
        while !state.is_ended() {
            if round_count > 100 {
                error!("round_count > 100; terminating game");
                return false;
            }
            // Try to draw tile
            for draw_retries in 0..=10 {
                if draw_retries == 10 {
                    // Revert push operations
                    states.truncate(states.len() - round_count);
                    pi_dists.truncate(states.len() - round_count);
                    return false;
                }

                if state.draw_and_get_legal_actions().len() != 0 {
                    break;
                }
                state.undraw_tile();
            }

            round_count += 1;

            // Randomize playout cap
            let (playout_cap, _weight) = PLAYOUT_CAPS
                .choose_weighted(&mut thread_rng(), |(_value, weight)| *weight)
                .unwrap();

            // Query action and record
            let (action, pi_dist) = agent.query_mcts(state.clone(), *playout_cap);
            states.push(state.clone());
            pi_dists.push(pi_dist);

            // Perform action
            state = state.get_state_after_action(action);
        }

        for _ in 0..round_count {
            winners.push(state.get_winner());
            scores.push(state.get_scores().clone());
        }

        true
    }

    fn create_testing_model_if_not_exists() {
        if !std::path::Path::new(TRAINING_MODEL_PATH).exists() {
            info!("Duplicating baseline model as training model");
            std::process::Command::new("cp")
                .arg("-r")
                .arg(BASELINE_MODEL_PATH)
                .arg(TRAINING_MODEL_PATH)
                .output()
                .expect("Failed to copy model directory");
        }
    }

    pub fn perform_gate_test(num_matches: usize) {
        let mut baseline_model =
            NNMCTSAgent::new(BASELINE_MODEL_PATH).expect("Failed to init baseline model");
        let mut training_model =
            NNMCTSAgent::new(TRAINING_MODEL_PATH).expect("Failed to init training model");
        baseline_model.set_playout_cap(250);
        training_model.set_playout_cap(250);
        let gate_test_passed = if let [baseline_score, training_score, tie_score] = test_agents(
            Box::new(baseline_model),
            Box::new(training_model),
            false, // Don't save images
            true,  // Early stop
            num_matches,
        )[0..3]
        {
            info!(
                "Gate test result: {} vs {} (tie {})",
                baseline_score, training_score, tie_score
            );
            if training_score >= baseline_score {
                true
            } else {
                false
            }
        } else {
            panic!("Impossible branch");
        };

        if gate_test_passed {
            info!("Gate test passed, replacing model");

            std::fs::remove_dir_all(BASELINE_MODEL_PATH)
                .expect("Failed to remove old baseline model");

            // Ensure generations path
            let _ = std::fs::create_dir_all(GENERATIONS_PATH);
            let last_gen_num = std::fs::read_dir(GENERATIONS_PATH)
                .expect("Failed to read generations directory")
                .filter_map(|entry| {
                    entry
                        .ok()
                        .and_then(|entry| entry.file_name().to_str().map(|s| s.to_string()))
                })
                .filter(|name| name.starts_with("model-"))
                .map(|name| name["model-".len()..].parse::<usize>().unwrap_or(0))
                .max()
                .unwrap_or(0);

            info!(
                "Copying training model as new baseline model at `{}`",
                BASELINE_MODEL_PATH
            );
            std::process::Command::new("cp")
                .arg("-r")
                .arg(TRAINING_MODEL_PATH)
                .arg(BASELINE_MODEL_PATH)
                .output()
                .expect("Failed to copy model directory");
            let gen_backup_path = format!("generations/model-{}", last_gen_num + 1);
            info!("Backing up new baseline model to `{}`", gen_backup_path);
            std::process::Command::new("cp")
                .arg("-r")
                .arg(TRAINING_MODEL_PATH)
                .arg(gen_backup_path)
                .output()
                .expect("Failed to copy model directory");
        }
    }

    /// Conduct test matches between the agents. If `save_imgs` is true, then the first match will
    /// be saved as images in `test-match/`
    pub fn test_agents(
        mut agent_a: Box<dyn Agent<S = ccs::State, A = ccs::Action>>,
        mut agent_b: Box<dyn Agent<S = ccs::State, A = ccs::Action>>,
        save_imgs: bool,
        early_stop: bool,
        num_matches: usize,
    ) -> Vec<usize> {
        // Try to create test image directory
        if save_imgs {
            let _ = std::fs::create_dir_all("test-match");
        }

        if num_matches % 2 != 0 {
            panic!("`test_agents` must be called with even `num_matches`");
        }

        let mut win_matches = vec![0; 3];

        let mut match_id = 0;
        while match_id < num_matches {
            if early_stop && (win_matches[0] > num_matches / 2 || win_matches[1] > num_matches / 2)
            {
                info!("One of the agents won over half of the games. Aborting.");
                break;
            }
            if let Some(result) = test_agent(&mut agent_a, &mut agent_b, save_imgs && match_id == 0)
            {
                println!("Result: {}", result);
                if result == 2 {
                    win_matches[2] += 1;
                } else {
                    let invert = match_id % 2 == 0;
                    if invert {
                        win_matches[result] += 1;
                    } else {
                        win_matches[1 - result] += 1;
                    }
                }
                std::mem::swap(&mut agent_a, &mut agent_b);
                match_id += 1;
                eprintln!("RESULT\n{:#?}", win_matches);
            } else {
                eprintln!("Restart match {}", match_id);
            }
        }

        win_matches
    }

    /// Conduct test match between the agents. If `save_imgs` is true, then every step will be
    /// saved as an image as `test-match/%d.png`.
    fn test_agent(
        agent_a: &mut Box<dyn Agent<S = ccs::State, A = ccs::Action>>,
        agent_b: &mut Box<dyn Agent<S = ccs::State, A = ccs::Action>>,
        save_imgs: bool,
    ) -> Option<usize> {
        let mut state = ccs::State::new();

        let mut it = 0;
        let mut players = [ccs::Player::Red, ccs::Player::Blue];

        if save_imgs {
            if let Err(e) = state.save_img("test-match/0.png") {
                warn!("Failed to save image: {:?}", e);
            }
        }

        while !state.is_ended() {
            it += 1;
            info!("Round {}", it);

            for draw_retries in 0..=10 {
                if draw_retries == 10 {
                    return None;
                }

                if state.draw_and_get_legal_actions().len() != 0 {
                    break;
                }
                state.undraw_tile();
            }

            let action = if it % 2 == 1 {
                agent_a.query_action(&state)
            } else {
                agent_b.query_action(&state)
            };

            state = state.get_state_after_action(action);

            if save_imgs {
                if let Err(e) = state.save_img(&format!("test-match/{}.png", it)) {
                    warn!("Failed to save image: {:?}", e);
                }
            }

            players.swap(0, 1);
        }

        info!(
            "Winner: {:?}; Scores: {:?}",
            state.get_winner(),
            state.get_scores()
        );
        Some(match state.get_winner() {
            Some(player) => player.idx(),
            None => 2,
        })
    }

    pub fn get_agent_from_str<S: AsRef<str>>(
        name: S,
    ) -> Option<Box<dyn Agent<S = ccs::State, A = ccs::Action>>> {
        let agent = match name.as_ref() {
            "training" => {
                Box::new(NNMCTSAgent::new(TRAINING_MODEL_PATH).expect("Failed to create model"))
                    as Box<dyn Agent<S = ccs::State, A = ccs::Action>>
            }
            "baseline" => {
                Box::new(NNMCTSAgent::new(BASELINE_MODEL_PATH).expect("Failed to create model"))
                    as Box<dyn Agent<S = ccs::State, A = ccs::Action>>
            }
            "rand" => Box::new(RandAgent) as Box<dyn Agent<S = ccs::State, A = ccs::Action>>,
            "mcts" => Box::new(MCTSAgent) as Box<dyn Agent<S = ccs::State, A = ccs::Action>>,
            _ => return None,
        };
        Some(agent)
    }
}

mod logging {
    use simplelog::*;

    pub fn init_logging() {
        CombinedLogger::init(vec![
            TermLogger::new(LevelFilter::Info, Config::default(), TerminalMode::Mixed),
            WriteLogger::new(
                LevelFilter::Info,
                Config::default(),
                std::fs::OpenOptions::new()
                    .append(true)
                    .create(true)
                    .open("ccs.log")
                    .unwrap(),
            ),
        ])
        .unwrap()
    }
}

fn main() {
    logging::init_logging();

    let matches = App::new("Carcassonne")
        .version("0.1")
        .author("Akitaki <robinhuang123@gmail.com>")
        .subcommand(
            SubCommand::with_name("train")
                .about("Train an epoch")
                .arg(
                    Arg::with_name("plays")
                        .short("p")
                        .value_name("PLAYS")
                        .required(true),
                )
                .arg(
                    Arg::with_name("threads")
                        .short("t")
                        .value_name("THREADS")
                        .required(true),
                )
                .arg(
                    Arg::with_name("gate-matches")
                        .short("g")
                        .value_name("GATE_MATCHES")
                        .required(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("test")
                .about("Test agents")
                .arg(
                    Arg::with_name("first-agent")
                        .short("a")
                        .value_name("FIRST_AGENT")
                        .required(true),
                )
                .arg(
                    Arg::with_name("second-agent")
                        .short("b")
                        .value_name("SECOND_AGENT")
                        .required(true),
                )
                .arg(
                    Arg::with_name("num-matches")
                        .short("n")
                        .value_name("NUM_MATCHES")
                        .default_value("10"),
                )
                .arg(Arg::with_name("save-images").short("i").takes_value(false)),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("train") {
        let selfplay_times = matches
            .value_of("plays")
            .unwrap()
            .parse::<usize>()
            .expect("Failed to parse PLAYS");
        let selfplay_threads = matches
            .value_of("threads")
            .unwrap()
            .parse::<usize>()
            .expect("Failed to parse THREADS");
        let gate_matches = matches
            .value_of("gate-matches")
            .unwrap()
            .parse::<usize>()
            .expect("Failed to parse THREADS");
        if gate_matches % 2 != 0 {
            error!("Gate matches must be divisible by 2");
            return;
        }
        training::train_epoch(selfplay_times, selfplay_threads);
        training::perform_gate_test(gate_matches);
    }

    if let Some(matches) = matches.subcommand_matches("test") {
        let agent_name_a = matches.value_of("first-agent").unwrap();
        let agent_name_b = matches.value_of("second-agent").unwrap();
        let rounds = matches
            .value_of("num-matches")
            .unwrap()
            .parse::<usize>()
            .expect("Failed to parse NUM_MATCHES");
        let should_save_images = matches.is_present("save-images");

        training::test_agents(
            training::get_agent_from_str(agent_name_a).expect("Failed to get agent A"),
            training::get_agent_from_str(agent_name_b).expect("Failed to get agent B"),
            should_save_images,
            false, // Don't early stop
            rounds,
        );
    }
}
