use indextree::Arena;
use ord_subset::OrdSubsetIterExt;
use rand::prelude::*;
use std::rc::Rc;

#[derive(Debug)]
enum MaybeState<S, A> {
    State(Rc<S>),
    Action(Rc<S>, A),
}

impl<S, A> MaybeState<S, A> {
    fn from_state(state: S) -> Self {
        MaybeState::State(Rc::new(state))
    }

    fn from_parent_and_action(parent_state: Rc<S>, action: A) -> Self {
        MaybeState::Action(parent_state, action)
    }

    fn convert_to_state<F: FnOnce(&S, &A) -> S>(&mut self, state_action_to_state: F) {
        if let MaybeState::Action(parent_state, action) = self {
            *self = MaybeState::State(Rc::new(state_action_to_state(parent_state, action)));
        }
    }
}

#[derive(Debug)]
struct Node<S, A> {
    maybe_state: MaybeState<S, A>,
    visit_count: usize,
    value: f64,
}

impl<S, A> Node<S, A> {
    fn from_state(state: S) -> Self {
        Self {
            maybe_state: MaybeState::from_state(state),
            visit_count: 0,
            value: 0f64,
        }
    }

    fn from_parent_and_action(parent: Rc<S>, action: A) -> Self {
        Self {
            maybe_state: MaybeState::from_parent_and_action(parent, action),
            visit_count: 0,
            value: 0f64,
        }
    }

    fn convert_to_state<F: FnOnce(&S, &A) -> S>(&mut self, state_action_to_state: F) {
        self.maybe_state.convert_to_state(state_action_to_state);
    }

    fn unwrap_state(&self) -> Rc<S> {
        if let MaybeState::State(state) = &self.maybe_state {
            state.clone()
        } else {
            panic!("Attempted to unwrap state on MaybeState::Action variant");
        }
    }
}

impl<S, A> std::fmt::Display for Node<S, A> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Node ( vc: {}, v: {} )", self.visit_count, self.value)
    }
}

pub trait MCTS {
    /// The state type, used to store a game state (including last move & next player information).
    type S: Clone + std::fmt::Debug;
    /// The action type, mainly used as return type of [`MCTS::query_mcts`].
    type A;
    /// The player type, used to determine next player, winner etc.
    type P: PartialEq + Copy;
    /// The "state iterator" type, used as the return type of [`MCTS::get_derivatives_from_state`].
    type SI: Iterator<Item = (Self::S, Self::A)>;

    fn get_next_player_from_state(state: Self::S) -> Self::P;
    fn get_winner_from_state(state: Self::S) -> Option<Self::P>;
    fn get_is_ended_from_state(state: &Self::S) -> bool;
    // fn get_is_winner_opponent_of(state: &Self::S, player: Self::P) -> bool;
    fn get_derivatives_from_state(state: &Self::S) -> Self::SI;
    fn get_last_action_from_state(state: &Self::S) -> Option<Self::A>;
    fn get_random_derivative_from_state(state: &Self::S) -> Option<Self::S>;
    fn get_result(state: &Self::S, action: &Self::A) -> Self::S;

    /// Query the optimal action of type `A` from a state `S`.
    /// The goal is to maximize the win rate of `get_next_player_from_state(starting_from_state)`
    /// (see [`MCTS::get_next_player_from_state`])
    fn query_mcts(&self, starting_from_state: &Self::S) -> Self::A {
        let mut search_tree = Arena::new();
        let me_player = Self::get_next_player_from_state(starting_from_state.clone());
        let tree_root = search_tree.new_node(Node::<Self::S, Self::A>::from_state(
            starting_from_state.clone(),
        ));

        for _ in 0..1000 {
            // Selection phase
            let mut cur_node = tree_root;
            // while cur_node has at least a child, proceed down
            while cur_node.children(&search_tree).next().is_some() {
                let parent_visit_count =
                    search_tree.get(cur_node).unwrap().get().visit_count as f64;
                // out of childrens of cur_node, choose the one with maximum UCB value,
                // and then assign as the new cur_node
                cur_node = cur_node
                    .children(&search_tree)
                    .ord_subset_max_by_key(|&child| {
                        let child_node = search_tree.get(child).unwrap().get();
                        let visit_count = child_node.visit_count as f64;
                        let value = child_node.value / visit_count;
                        if visit_count != 0.0 {
                            value + 2f64.sqrt() * (parent_visit_count.ln() / visit_count).sqrt()
                        } else {
                            f64::MAX
                        }
                    })
                    .unwrap();
            }

            // Expansion phase
            // if cur_node's state is non-ending state
            if cur_node == tree_root || search_tree.get(cur_node).unwrap().get().visit_count > 10 {
                let leaf_node = search_tree.get_mut(cur_node).unwrap().get_mut();
                leaf_node.convert_to_state(|state, action| Self::get_result(state, action));

                let leaf_node_state_is_ended =
                    Self::get_is_ended_from_state(leaf_node.unwrap_state().as_ref());

                if !leaf_node_state_is_ended {
                    // clone cur_node's state
                    let leaf_state = leaf_node.unwrap_state();

                    // find all derived states from cur_node's state, and then append to
                    // cur_node
                    for (_derived_state, deriving_action) in
                        Self::get_derivatives_from_state(leaf_state.as_ref())
                    {
                        let derived_nodeid = search_tree.new_node(Node::from_parent_and_action(
                            leaf_state.clone(),
                            deriving_action,
                        ));
                        cur_node.append(derived_nodeid, &mut search_tree);
                    }

                    // move down from cur_node to a randomly chosen child
                    if cur_node.children(&search_tree).next().is_some() {
                        cur_node = cur_node
                            .children(&search_tree)
                            .choose(&mut thread_rng())
                            .unwrap();
                    }
                }
            }

            // Expand that chosen `cur_node`
            search_tree
                .get_mut(cur_node)
                .unwrap()
                .get_mut()
                .convert_to_state(|state, action| Self::get_result(state, action));

            // Simulation
            // clone cur_node's state
            let mut state = search_tree
                .get(cur_node)
                .unwrap()
                .get()
                .unwrap_state()
                .as_ref()
                .clone();

            // while state is a non-ending state
            while !Self::get_is_ended_from_state(&state) {
                // advance the state randomly
                if let Some(next_state) = Self::get_random_derivative_from_state(&state) {
                    state = next_state
                } else {
                    break;
                }
            }

            // Backprop
            let score = match Self::get_winner_from_state(state) {
                Some(winner) => {
                    if winner == me_player {
                        1f64
                    } else {
                        0f64
                    }
                }
                None => 0.75f64,
            };

            let mut cur_node_opt = Some(cur_node);
            // while cur_node_opt is not None
            while let Some(cur_node) = cur_node_opt {
                {
                    let cur_node = search_tree.get_mut(cur_node).unwrap().get_mut();
                    cur_node.visit_count += 1;
                    cur_node.value += score;
                }
                // set cur_node_opt to cur_node's parent if it exists
                // otherwise None is given (effectively terminating the loop)
                if let Some(parent_node) = search_tree.get(cur_node).unwrap().parent() {
                    cur_node_opt = Some(parent_node);
                } else {
                    cur_node_opt = None;
                }
            }
        }

        if let Some(best_child_node_id) = tree_root
            .children(&search_tree)
            .max_by_key(|&child_id| search_tree.get(child_id).unwrap().get().visit_count)
        {
            let best_child_state = search_tree
                .get(best_child_node_id)
                .unwrap()
                .get()
                .unwrap_state();
            let best_child_last_action =
                Self::get_last_action_from_state(best_child_state.as_ref()).unwrap();
            best_child_last_action
        } else {
            eprintln!("Fallback to random; maybe it's a bug?");
            Self::get_derivatives_from_state(starting_from_state)
                .choose(&mut thread_rng())
                .unwrap()
                .1
        }
    }
}
