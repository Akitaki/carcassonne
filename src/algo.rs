mod mcts;
mod nn_mcts;

// Re-export algorithm traits
pub use mcts::MCTS;
pub use nn_mcts::NNMCTS;
